variable "infrastructure_version" {
  default = "1"
}

provider "aws" {
  region = "eu-central-1"
}


terraform {
  backend "s3" {
    encrypt = true
    bucket  = "terraform-blue"
    region  = "eu-central-1"
    dynamodb_table = "tfstate-temy"
    shared_credentials_file = "$HOME/.aws/credentials"
    key     = "v1"
  }
  required_providers {
    aws     = ">= 2.14.0"
  }
}


resource "aws_vpc" "Blue" {
  cidr_block       = var.cidr_block
  instance_tenancy = "default"

  tags = {
    Name = "${var.tag}-main"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.Blue.id
  tags = {
    Name = "${var.tag}-main"
  }
}


resource "aws_subnet" "public_subnet_prod-pub" {
  vpc_id     = aws_vpc.Blue.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "${var.tag}-pub"
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.Blue.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }
  tags =  {
    Name = "${var.tag}-pub"
  }
}

resource "aws_route_table_association" "prod-pub" {
  subnet_id = aws_subnet.public_subnet_prod-pub.id
  route_table_id = aws_route_table.public.id
}
