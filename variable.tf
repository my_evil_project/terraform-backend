variable "region" {
  default = "us-central-1"
}

variable "profile" {
  default = "default"
}

variable "cidr_block" {
  default  = "10.0.0.0/16"
}

variable "tag" {
  default  = "prod"
}

